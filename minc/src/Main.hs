module Main where

import Language.Mincaml.Codegen
import Language.Mincaml.Parser
import Language.Mincaml.Resolve
import Language.Mincaml.Typing
import Language.Mincaml.Closure

import LLVM.General.Module
import LLVM.General.Context

import LLVM.General.AST
import LLVM.General.AST.Global

import Text.Parsec
import Control.Monad.Error

main :: IO ()
main = do
  input <- getContents 
  case parse parseMinCaml "stdin" input of
    Left err -> do
      putStr "Parse error:"
      print err 
    Right expr ->     
      showLlvmAssembly (genCode $ addClosures $ typecheck $ resolve expr)

showLlvmAssembly astModule = withContext $ \ctx -> do
  res <- runErrorT $ withModuleFromAST ctx astModule $ \mod -> do
    assembly <- moduleLLVMAssembly mod 
    putStrLn assembly
  case res of 
    Left err -> do
      putStr "Error when creating llvm-assembly:"
      print err
    Right _ -> 
      return ()
