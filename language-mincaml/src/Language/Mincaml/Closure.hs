module Language.Mincaml.Closure (
  addClosures,
  findClosures,
  freeVars
)
where

import Control.Applicative
import Control.Monad
import Control.Monad.State.Strict
import Data.Maybe
import qualified Data.Map.Strict as Map
import qualified Data.Set as Set

import Language.Mincaml.Syntax
import Control.Lens


freeVars (ConstantExpr _)               = Set.empty
freeVars (UnaryExpr _ arg)              = freeVars arg
freeVars (BinaryExpr larg _ rarg)       = (freeVars larg) `Set.union` (freeVars rarg)
freeVars (IfExpr cond th el)            = Set.unions $ map freeVars [cond, th, el]
freeVars (ResolvedVarExpr var)          = Set.singleton var
freeVars (ResolvedLetExpr var init body) = Set.delete var
  ((freeVars init) `Set.union` (freeVars body)) 
freeVars (ResolvedLetRecExpr var params init body) = 
  ((freeVars init) `Set.union` (freeVars body)) `Set.difference` (Set.fromList (var:params))
freeVars (ApplyExpr f args)          = Set.unions $ map freeVars (f:args)
freeVars (TupleExpr elems)           = Set.unions $ map freeVars elems
freeVars (ArrayCreateExpr size init) = (freeVars size) `Set.union` (freeVars init)
freeVars (ArrayReadExpr arr ix)      = (freeVars arr) `Set.union` (freeVars ix)
freeVars (ArrayWriteExpr arr ix val) = Set.unions $ map freeVars [arr, ix, val]

findClosures (ConstantExpr _)         = Map.empty
findClosures (ResolvedVarExpr var)    = Map.empty
findClosures (UnaryExpr _ arg)        = findClosures arg
findClosures (BinaryExpr larg _ rarg) 
  = (findClosures larg) `Map.union` (findClosures rarg)
findClosures (IfExpr cond th el)            
  = Map.unions $ map findClosures [cond, th, el]
findClosures (ResolvedLetExpr var init body) = 
  ((findClosures init) `Map.union` (findClosures body)) 
findClosures x@(ResolvedLetRecExpr var params init body) 
  = Map.insert var (Set.toList (freeVars x))
    ((findClosures init) `Map.union` (findClosures body)) 
findClosures (ApplyExpr f args)          = Map.unions $ map findClosures (f:args)
findClosures (TupleExpr elems)           = Map.unions $ map findClosures elems
findClosures (ArrayCreateExpr size init) = (findClosures size) `Map.union` (findClosures init)
findClosures (ArrayReadExpr arr ix)      = (findClosures arr) `Map.union` (findClosures ix)
findClosures (ArrayWriteExpr arr ix val) = Map.unions $ map findClosures [arr, ix, val]


addClosures node = foldExpr (addClosure (findClosures node)) node
  where
    addClosure _ (ResolvedLetExpr var init body) 
      = ResolvedLetExpr (var & vdClosure .~ []) init body
    addClosure cls (ResolvedVarExpr var) 
      = ResolvedVarExpr (var & vdClosure .~ (maybe [] id $ Map.lookup var cls))
    addClosure cls (ResolvedLetRecExpr var params init body) 
      = ResolvedLetRecExpr 
          (var & vdClosure .~ (maybe [] id $ Map.lookup var cls)) 
          params 
          init 
          body
    addClosure _ x = x
