{-# LANGUAGE TemplateHaskell #-}
module Language.Mincaml.Typing (
  typecheck,
  getType
)
where

import Control.Applicative
import Control.Monad
import Control.Monad.State.Strict
import qualified Data.Map.Strict as Map

import Language.Mincaml.Syntax
import Control.Lens

type TypeEnvironment = Map.Map TypeVarDecl Type

data TypeCheckingState = TypeCheckingState {
    _tcsEnv :: TypeEnvironment,
    _tcsVarEnv :: Map.Map VarDecl TypeVarDecl,
    _tcsNextTypeId :: UniqueId
  } deriving Show

makeLenses ''TypeCheckingState

type TypecheckingM = State TypeCheckingState

tyVarContents :: TypeVarDecl -> TypecheckingM (Maybe Type)
tyVarContents td = use $ tcsEnv . at td 

newTyVar :: TypecheckingM TypeVarDecl
newTyVar = do
  name <- use tcsNextTypeId
  tcsNextTypeId += 1
  return $ TypeVarDecl name  

getTypeOfVar :: VarDecl -> TypecheckingM Type 
getTypeOfVar var = if var ^. vdUid == UniqueId 0 then return $ var ^. vdType else do
  lookupRes <- use (tcsVarEnv . at var)
  case lookupRes of
    Just ty -> return $ TyVar ty
    Nothing -> do  
      tvar <- newTyVar
      modify (\s -> s & tcsVarEnv . at var ?~ tvar)
      return $ TyVar tvar


learnSubstitution :: TypeVarDecl -> Type -> TypecheckingM ()
learnSubstitution tvar ty = modify (\env -> env & (tcsEnv . at tvar) ?~ ty)
  -- TODO: do the occurs check

handleTyVar :: Maybe Type -> TypeVarDecl -> Type -> TypecheckingM ()
handleTyVar Nothing x ty = learnSubstitution x ty
handleTyVar (Just ty') _ ty = unify ty' ty

unify :: Type -> Type -> TypecheckingM ()
unify (TyVar x) rhs@(TyVar y)
  = unless (x == y) $ do
      xcont <- tyVarContents x
      ycont <- tyVarContents y
      case (xcont, ycont) of
        (Just xt, _) -> handleTyVar ycont y xt
        (_, Just yt) -> handleTyVar xcont x yt
        _ -> handleTyVar Nothing x rhs
unify (TyVar x) ty = do
  cont <- tyVarContents x
  handleTyVar cont x ty
unify tx (TyVar y) = do
  cont <- tyVarContents y
  handleTyVar cont y tx

unify (TyArray ty) (TyArray ty')            = unify ty ty'
unify (TyFunc pars ret) (TyFunc pars' ret') =
  if length pars /= length pars' 
    then fail "Unification failed: function types have different parameter count"
    else do 
      mapM_ (uncurry unify) (zip pars pars')
      unify ret ret'
unify (TyTuple ts) (TyTuple ts') =
  if length ts /= length ts' 
    then fail "Unification failed: tuple types have different element count"
    else mapM_ (uncurry unify) (zip ts ts')

unify lhs rhs = 
  unless (lhs == rhs) $ 
    fail $ "Unification failed: trying to unify different types.\n\t" 
      ++ show lhs ++ "\n\t" ++ show rhs
    
process :: Expression -> TypecheckingM Type
process (ConstantExpr (ConstInt _)) = return TyInt
process (ConstantExpr (ConstFloat _)) = return TyFloat

process (UnaryExpr op e) =
  process e >>= unify opTy >> return opTy
  where
    opTy = getTypeByOp op
    getTypeByOp UnMinus = TyInt
    getTypeByOp UnNot   = TyBool

process (BinaryExpr lhs op rhs) = do
  lhsTy <- process lhs
  rhsTy <- process rhs
  unify lhsTy opTy
  unify rhsTy opTy
  return retTy
  where 
    (opTy, retTy) = getTypeByOp op

    getTypeByOp (BinPlus k)  = let r = getTypeByOpKind k in (r, r)
    getTypeByOp (BinMinus k) = let r = getTypeByOpKind k in (r, r)
    getTypeByOp (BinMult k)  = let r = getTypeByOpKind k in (r, r)
    getTypeByOp (BinDiv k)   = let r = getTypeByOpKind k in (r, r)
    getTypeByOp BinLess            = (TyInt, TyBool)
    getTypeByOp BinGreater         = (TyInt, TyBool)
    getTypeByOp BinEquals          = (TyInt, TyBool)
    getTypeByOp BinLessOrEquals    = (TyInt, TyBool)
    getTypeByOp BinGreaterOrEquals = (TyInt, TyBool)

    getTypeByOpKind PKInt   = TyInt
    getTypeByOpKind PKFloat = TyFloat

process (IfExpr cond th el) = do 
  condTy <- process cond
  thTy <- process th
  elTy <- process el
  unify condTy TyBool
  unify thTy elTy
  return thTy

process (ResolvedLetExpr var ass body) = do
  assTy <- process ass
  unify assTy =<< getTypeOfVar var
  process body

process (ResolvedLetRecExpr fun pars body rest) = do
  funTy <- getTypeOfVar fun
  parTys <- mapM getTypeOfVar pars
  bodyTy <- process body
  unify funTy (TyFunc parTys bodyTy)
  process rest

process (ResolvedVarExpr var) = getTypeOfVar var 

process (ApplyExpr fun args) = do
  funTy <- process fun
  argTys <- mapM process args
  retTy <- TyVar <$> newTyVar
  unify funTy (TyFunc argTys retTy)
  return retTy

process (TupleExpr [])  = return TyUnit
process (TupleExpr [e]) = process e
process (TupleExpr es)  = TyTuple <$> mapM process es

process (ArrayCreateExpr sz val) = do
  szTy <- process sz
  unify szTy TyInt
  valTy <- process val
  return $ TyArray valTy  

process (ArrayReadExpr arr index) = processArrayHead arr index

process (ArrayWriteExpr arr index ass) = do
  innerTy <- processArrayHead arr index
  assTy <- process ass
  unify assTy innerTy
  return innerTy 
  
processArrayHead arr index = do
  innerTy <- TyVar <$> newTyVar
  arrTy <- process arr
  unify arrTy (TyArray innerTy)
  indexTy <- process index
  unify indexTy TyInt
  return innerTy

runTypeChecking e 
  = let (t, s) = runState (process e) (TypeCheckingState Map.empty Map.empty (UniqueId 0))
    in (substituteType s t, s)
  
execTypeChecking = snd . runTypeChecking

getTypeByDecl :: TypeCheckingState -> TypeVarDecl -> Type
getTypeByDecl env tdecl 
  = case view (tcsEnv . at tdecl) env of
      Just t -> substituteType env t      
      Nothing -> TyInt

substituteType :: TypeCheckingState -> Type -> Type
substituteType env = foldType subst 
  where
    subst (TyVar decl) = getTypeByDecl env decl
    subst t = t
    
substitute :: TypeCheckingState -> Expression -> Expression
substitute env = foldExpr subst
  where
    getType var
      = case view (tcsVarEnv . at var) env of
          Just tdecl -> getTypeByDecl env tdecl 
          Nothing -> var ^. vdType --error "Couldn't find type-decl for var-decl"                  
    updateDecl var = var & vdType .~ getType var
    subst (ResolvedVarExpr var) = ResolvedVarExpr (updateDecl var)
    subst (ResolvedLetExpr var ass body) = ResolvedLetExpr (updateDecl var) ass body
    subst (ResolvedLetRecExpr fun pars body rest) 
      = ResolvedLetRecExpr (updateDecl fun) (map updateDecl pars) body rest
    subst e = e
  

typecheck :: Expression -> Expression
typecheck e = substitute (execTypeChecking e) e

getType :: Expression -> Type
getType = fst . runTypeChecking
