module Language.Mincaml.Parser(
  parseMinCaml
) where

import Language.Mincaml.Syntax

import Control.Applicative
import Data.List
import Text.Parsec hiding((<|>))
import Text.Parsec.Expr
import qualified Text.Parsec.Token as PT

parseMinCaml :: Parsec String u Expression
parseMinCaml = whitespace >> parseExpr <* eof

minCamlLanguageDef :: PT.LanguageDef st 
minCamlLanguageDef  = PT.LanguageDef {
    PT.commentStart    = "(*",
    PT.commentEnd      = "*)",
    PT.commentLine     = "",
    PT.nestedComments  = True,
    PT.identStart      = letter <|> char '_',
    PT.identLetter     = alphaNum <|> char '_',
    PT.opStart         = oneOf "+-*/=!<>",
    PT.opLetter        = oneOf ".=-",
    PT.reservedNames   = ["let", "rec", "in", "if", "then", "else", "true", "false", "Array.create"],
    PT.reservedOpNames = ["+", "-", "*", "/", "+.", "-.", "*.", "/.", "!", "<", "<=", ">", ">=", "=", "<-"],    
    PT.caseSensitive   = True
  }

minCamlLexer :: PT.TokenParser st
minCamlLexer = PT.makeTokenParser minCamlLanguageDef

whitespace   = PT.whiteSpace minCamlLexer
parens       = PT.parens minCamlLexer
symbol       = PT.symbol minCamlLexer
keyword      = PT.reserved minCamlLexer
identifier   = PT.identifier minCamlLexer
opTok     op = const op <$> PT.reservedOp minCamlLexer op

pOps ops = foldl1' (<|>) binOpParsers
  where
    binOpParsers = map opTok ops

pUnOp ops  = mkUnOp <$> pOps ops <?> "Unary opserator"
pBinOp ops = mkBinOp <$> pOps ops <?> "Binary opserator"

opTable 
  = [[Prefix (UnaryExpr <$> pUnOp ["!"])],
     [Prefix (UnaryExpr <$> pUnOp ["-"])],
     [Prefix (UnaryExpr <$> pUnOp ["-."])],
     [Infix (flip BinaryExpr <$> pBinOp ["*", "/", "*.", "/."]) AssocLeft],
     [Infix (flip BinaryExpr <$> pBinOp ["-.", "-", "+", "+."]) AssocLeft],
     [Infix (flip BinaryExpr <$> pBinOp ["<=", "<", ">=", ">", "="]) AssocLeft]]

parseExpr 
  =   try parseArrayWrite
  <|> buildExpressionParser opTable parseApplicationTerm

parseExprTerm 
  =   parseConstExpr
  <|> parseTupleExpr 
  <|> parseIfExpr 
  <|> parseVarExpr
  <|> parseLetExpr
  
parseArrayExprTerm
  =   try parseArrayRead
  <|> parseExprTerm

parseApplicationTerm
  =   try parseApplyExpr
  <|> parseArrayExprTerm

parseConstExpr
  = ConstantExpr <$> pConst

parseTupleExpr 
  = TupleExpr <$> pExprList 

parseArrayCreate 
  = ArrayCreateExpr 
      <$> (keyword "Array.create" *> parseExprTerm)
      <*> parseExprTerm
    <?> "Array.create expression"

parseArrayAccess
  = do h <- parseExprTerm
       pArrayAccessTail h
  where
    pArrayAccessTail h 
      = do t1 <- pArrayAccessTailItem
           pArrayAccessTail (ArrayReadExpr h t1) <|> return (h, t1)
    pArrayAccessTailItem = symbol "." *> parens parseExpr
    
parseArrayRead = uncurry ArrayReadExpr <$> parseArrayAccess
    <?> "Array read expression"    
  where
    pArrayReadTailItem = symbol "." *> parens parseExpr
    pArrayReadTail = many1 pArrayReadTailItem
    
parseArrayWrite 
  = uncurry ArrayWriteExpr     
      <$> parseArrayAccess
      <*> (symbol "<-" *> parseExpr)
    <?> "Array write expression"

parseIfExpr 
  = IfExpr
      <$> (keyword "if" *> parseExpr)
      <*> (keyword "then" *> parseExpr)
      <*> (keyword "else" *> parseExpr)
    <?> "Conditional expression"

parseVarExpr 
  = VarExpr <$> pVarIdent 
    <?> "Variable expression"

parseApplyExpr 
  =   parseArrayCreate
  <|> (ApplyExpr 
        <$> parseArrayExprTerm
        <*> many1 parseArrayExprTerm
       <?> "Application expression")

parseLetExpr 
  = keyword "let" >> (
        (LetRecExpr 
          <$> (keyword "rec" *> pVarIdent)
          <*> pFuncParameters
          <*> (symbol "=" *> parseExpr)
          <*> (keyword "in" *> parseExpr))
      <|>
        (LetExpr
          <$> pVarIdent
          <*> (symbol "=" *> parseExpr)
          <*> (keyword "in" *> parseExpr)))
    <?> "Let expression"

pVarIdent 
  = VarIdent <$> identifier  

pExprList 
  = parens (parseExpr `sepBy` symbol ",")

pFuncParameters
  = parens (pVarIdent `sepBy` symbol ",")

pConst
  = either ConstInt ConstFloat <$> PT.naturalOrFloat minCamlLexer 
