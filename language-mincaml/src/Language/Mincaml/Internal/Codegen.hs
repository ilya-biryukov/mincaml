{-# LANGUAGE GeneralizedNewtypeDeriving, TemplateHaskell, Rank2Types #-}
module Language.Mincaml.Internal.Codegen(
  GlobalCodegenState(),
  GlobalCodegenM,
  runGlobalCodegen,
  addGlobalDeclValue,
  addFunctionDefinition,

  CodegenState(),
  CodegenM,
  runCodegen,
  newRegName,
  newBlockName,
  addBlock,
  addNewBlock,
  addDeclValue,
  getDeclValue,

  BlockBuilderM,
  runBlockBuilder,
  addInstr,
  addDoInstr,
  addCurrentBlockWithNewBlockName,
) where

import Control.Applicative
import Control.Monad
import Control.Monad.State.Strict
import Control.Lens
import Data.Maybe
import qualified Data.Map.Strict as Map

import LLVM.General.AST
import LLVM.General.AST.Name
import LLVM.General.AST.Global
import LLVM.General.AST.Instruction
import LLVM.General.AST.Operand
import LLVM.General.AST.Constant
import qualified Data.DList as DL

import Language.Mincaml.Syntax

data NameGen = NameGen {
    _ngPref :: String,
    _ngCounter :: Integer
  }

makeLenses ''NameGen

mkNameGen :: String -> NameGen
mkNameGen = flip NameGen 0

genName :: NameGen -> (Name, NameGen)
genName ng = (Name $ (ng ^. ngPref) ++ show (ng ^. ngCounter), ng & ngCounter +~ 1)

genNameM :: MonadState s m => Lens' s NameGen -> m Name
genNameM nmGenLens = do
  gen <- use nmGenLens
  let (name, newGen) = genName gen
  nmGenLens .= newGen
  return name

data GlobalCodegenState = GlobalCodegenState {
    _csGlobalDefs :: DL.DList Definition,
    _csGlobalDeclValues :: Map.Map VarDecl Operand
  }

makeLenses ''GlobalCodegenState

newtype GlobalCodegenM a = GlobalCodegenM {
    unGlobalCodegenM :: State GlobalCodegenState a
  } deriving (Monad, Applicative, Functor, MonadState GlobalCodegenState)

runGlobalCodegen :: GlobalCodegenM () -> Module
runGlobalCodegen action = 
  let st           = execState (unGlobalCodegenM action) initialState
      moduleDefs   = view csGlobalDefs st
      initialState = GlobalCodegenState DL.empty Map.empty
  in defaultModule { moduleDefinitions = DL.toList moduleDefs }

addFunctionDefinition :: Definition -> GlobalCodegenM ()
addFunctionDefinition def = modify (over csGlobalDefs (`DL.snoc` def))

addGlobalDeclValue :: VarDecl -> Operand -> GlobalCodegenM ()
addGlobalDeclValue decl operand = modify (\s -> s & csGlobalDeclValues . at decl ?~ operand)  

data CodegenState = CodegenState {
    _csCurrentFuncName :: Name,
    _csRegNameGen :: NameGen,
    _csBlockNameGen :: NameGen,
    _csBasicBlocks :: DL.DList BasicBlock,
    _csDeclValues :: Map.Map VarDecl Operand 
  }

makeLenses ''CodegenState

newtype CodegenT m a = CodegenM {
    unCodegenM :: StateT CodegenState m a
  } deriving (Monad, Applicative, Functor, MonadState CodegenState, MonadTrans)

type CodegenM = CodegenT GlobalCodegenM

runCodegen :: Name -> [Parameter] -> LLVM.General.AST.Type -> CodegenM () -> GlobalCodegenM ()
runCodegen funName params retTy a = do 
  blocks <- getBlocks <$> execStateT (unCodegenM a) initialState 
  let func = functionDefaults { 
        basicBlocks = blocks, 
        returnType = retTy,
        name = funName,
        parameters = (params, False) } 
  addFunctionDefinition (GlobalDefinition func)
  where    
    initialState = CodegenState funName (mkNameGen "reg") (mkNameGen "block") DL.empty Map.empty
    getBlocks = view (csBasicBlocks . to DL.toList)

newRegName :: CodegenM Name
newRegName = genNameM csRegNameGen

newBlockName :: CodegenM Name
newBlockName = genNameM csBlockNameGen

addNewBlock :: (Name -> CodegenM BasicBlock) -> CodegenM ()
addNewBlock adder = newBlockName >>= adder >>= addBlock

addBlock :: BasicBlock -> CodegenM ()
addBlock block = modify (over csBasicBlocks (`DL.snoc` block))

addDeclValue :: VarDecl -> Operand -> CodegenM ()
addDeclValue decl value = modify (\s -> s & csDeclValues . at decl ?~ value) 

getDeclValue :: VarDecl -> CodegenM (Maybe Operand)
getDeclValue decl = do 
  v <- use (csDeclValues . at decl)
  if isJust v 
      then return v
      else lift $ use (csGlobalDeclValues . at decl)

data BlockBuilderState = BlockBuilderState  {
    _bbName :: Name,
    _bbInstructions :: DL.DList (Named Instruction)
  }

makeLenses ''BlockBuilderState

newtype BlockBuilderT m a = BlockBuilderT {
    unBlockBuilder :: StateT BlockBuilderState m a
  } deriving (Monad, MonadState BlockBuilderState, MonadTrans, Functor, Applicative)

type BlockBuilderM = BlockBuilderT CodegenM

runBlockBuilder :: Name -> BlockBuilderM (a, Named Terminator) -> CodegenM (Name, a)
runBlockBuilder name action = flip evalStateT (BlockBuilderState name DL.empty) 
  $ unBlockBuilder $ do
      (val, term) <- action
      newName <- addCurrentBlock term
      return (newName, val)

buildBasicBlock :: Named Terminator -> BlockBuilderState -> BasicBlock
buildBasicBlock terminator state = 
  let instructions = view (bbInstructions . to DL.toList) state
      name         = view bbName state
  in 
      BasicBlock name instructions terminator

addInstr :: Instruction -> BlockBuilderM Operand
addInstr instr = do
  name <- lift newRegName
  modify (over bbInstructions (`DL.snoc` (name := instr)))
  return $ LocalReference name

addDoInstr :: Instruction -> BlockBuilderM ()
addDoInstr instr = modify (over bbInstructions (`DL.snoc` Do instr))

addCurrentBlock :: Named Terminator -> BlockBuilderM Name
addCurrentBlock term = do
  name <- use bbName
  addCurrentBlockWithNewBlockName term =<< lift newBlockName
  return name

addCurrentBlockWithNewBlockName :: Named Terminator -> Name -> BlockBuilderM ()
addCurrentBlockWithNewBlockName term newName = do 
  use (to (buildBasicBlock term)) >>= lift . addBlock
  modify $ const (BlockBuilderState newName DL.empty)
