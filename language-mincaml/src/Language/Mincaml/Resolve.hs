{-# LANGUAGE TemplateHaskell, Rank2Types #-}
module Language.Mincaml.Resolve (
  resolve  
) where

import Language.Mincaml.Syntax

import Control.Monad.State.Strict
import Control.Applicative
import Control.Lens hiding (Context)

import qualified Data.Map.Strict as Map

import Data.List

type Context = Map.Map VarIdent VarDecl

data ResolveState = ResolveState {
    _rsCtx :: Context,
    _rsNextVarId :: UniqueId,
    _rsNextTypeId :: UniqueId
  }

makeLenses ''ResolveState 

type ResolveMonad = State ResolveState

newRawId :: Lens' ResolveState UniqueId -> ResolveMonad UniqueId
newRawId nameLens = do 
  name <- use nameLens
  nameLens += 1
  return name

newTypeId = newRawId rsNextVarId
newVarId = newRawId rsNextTypeId

newVarDeclWithTy :: VarIdent -> Type -> ResolveMonad VarDecl
newVarDeclWithTy var ty 
  = VarDecl var 
    <$> newVarId 
    <*> pure ty
    <*> pure []

newVarDecl :: VarIdent -> ResolveMonad VarDecl
newVarDecl var = newVarDeclWithTy var =<< (TyVar . TypeVarDecl <$> newTypeId)

guardCtx :: ResolveMonad a -> ResolveMonad a 
guardCtx action = do
  ctx <- use rsCtx
  result <- action
  rsCtx .= ctx  
  return result

safeResolve' :: Expression -> ResolveMonad Expression
safeResolve' = guardCtx . resolve'

appendVarDeclToState :: ResolveState -> VarDecl -> ResolveState
appendVarDeclToState s v = s & rsCtx . at (view vdName v) ?~ v

appendVarDeclsToState :: ResolveState -> [VarDecl] -> ResolveState
appendVarDeclsToState = foldl' appendVarDeclToState

resolve' :: Expression -> ResolveMonad Expression
resolve' e@(ConstantExpr _)     = return e
resolve' (UnaryExpr op e1)      = UnaryExpr op <$> safeResolve' e1
resolve' (BinaryExpr e1 op e2)  = BinaryExpr <$> safeResolve' e1 <*> return op <*> safeResolve' e2
resolve' (IfExpr cond th el)    = IfExpr <$> safeResolve' cond <*> safeResolve' th <*> safeResolve' el
resolve' (LetExpr var ass body) = do
    newVar <- newVarDecl var
    ResolvedLetExpr newVar 
      <$> safeResolve' ass 
      <*> do
        modify (`appendVarDeclToState` newVar)
        resolve' body
resolve' (ResolvedLetExpr {}) =fail "Expression has already been resolved"
resolve' (LetRecExpr fun params body rest) = do
    funDecl <- newVarDecl fun
    paramDecls <- mapM newVarDecl params
    modify (`appendVarDeclToState` funDecl)
    ResolvedLetRecExpr funDecl paramDecls
      <$> guardCtx (do
            modify (`appendVarDeclsToState` paramDecls)
            resolve' body
          )
      <*> resolve' rest
resolve' (ResolvedLetRecExpr {}) =fail "Expression has already been resolved"
resolve' (VarExpr var) = do
  Just decl <- use (rsCtx . at var)
  return $ ResolvedVarExpr decl
resolve' (ResolvedVarExpr _) = fail "Expression has already been resolved"
resolve' (ApplyExpr e args)  = ApplyExpr <$> safeResolve' e <*> mapM safeResolve' args
resolve' (TupleExpr exprs)   = TupleExpr <$> mapM safeResolve' exprs
resolve' (ArrayCreateExpr size init) 
  = ArrayCreateExpr <$> safeResolve' size <*> safeResolve' init
resolve' (ArrayReadExpr arr ind) 
  = ArrayReadExpr <$> safeResolve' arr <*> safeResolve' ind
resolve' (ArrayWriteExpr arr ind val)
  = ArrayWriteExpr <$> safeResolve' arr <*> safeResolve' ind <*> safeResolve' val


addPrintFunctions :: ResolveMonad ()
addPrintFunctions = do
  printInt <- newVarDeclWithTy (VarIdent "printInt") (TyFunc [TyInt] TyUnit)
  modify (`appendVarDeclToState` printInt)


resolve :: Expression -> Expression
resolve e = evalState (addPrintFunctions >> resolve' e) 
  $ ResolveState Map.empty (UniqueId 0) (UniqueId 0)
