{-# LANGUAGE TemplateHaskell, GeneralizedNewtypeDeriving #-}
module Language.Mincaml.Syntax where

import Control.Lens hiding(Const)
import Data.Function(on)

data UnPrimOp = UnNot | UnMinus
  deriving (Eq, Show)

mkUnOp :: String -> UnPrimOp
mkUnOp "!" = UnNot
mkUnOp "-" = UnMinus
mkUnOp x   = error ("Unrecognized unary operator: " ++ x)

data PrimKind = PKFloat | PKInt
  deriving (Eq, Show)

data BinPrimOp 
  = BinPlus PrimKind
  | BinMinus PrimKind
  | BinMult PrimKind
  | BinDiv PrimKind
  | BinLess
  | BinGreater
  | BinLessOrEquals
  | BinGreaterOrEquals
  | BinEquals
  deriving (Eq, Show)

mkBinOp :: String -> BinPrimOp
mkBinOp a 
  = if last a == '.' 
      then mkBinOp' (init a) PKFloat
      else mkBinOp' a PKInt
  where
    mkBinOp' "+" pk = BinPlus pk
    mkBinOp' "-" pk = BinMinus pk
    mkBinOp' "*" pk = BinMult pk
    mkBinOp' "/" pk = BinDiv pk
    mkBinOp' "<"  _ = BinLess
    mkBinOp' "<=" _ = BinLessOrEquals
    mkBinOp' ">"  _ = BinGreater
    mkBinOp' ">=" _ = BinGreaterOrEquals
    mkBinOp' "="  _ = BinEquals
    mkBinOp' x    _ = error ("Unrecognied binary operator: " ++ x)

data Const 
  = ConstInt Integer
  | ConstFloat Double
  deriving (Eq, Show)

newtype VarIdent = VarIdent { 
    unVarIdent :: String
  } deriving (Eq, Show, Ord)

newtype UniqueId = UniqueId Int
  deriving (Eq, Show, Ord, Num)

data TypeVarDecl = TypeVarDecl {
    _tvdUid :: UniqueId
  } deriving (Eq, Show)

makeLenses ''TypeVarDecl

instance Ord TypeVarDecl where
  compare = compare `on` view tvdUid

data Type 
  = TyUnit
  | TyBool
  | TyInt
  | TyFloat
  | TyFunc {
      _ftParameterTypess :: [Type],
      _ftReturnType :: Type
    }
  | TyTuple {
      _tupleTypes :: [Type]
    }
  | TyArray {
      _arrInnerType :: Type
    }
  | TyVar {
      _typeVarDecl :: TypeVarDecl
    } 
  deriving (Eq, Show)

makeLenses ''Type

foldType :: (Type -> Type) -> Type -> Type
foldType f (TyFunc pars ret) = f (TyFunc (map (foldType f) pars) (foldType f ret))
foldType f (TyTuple ts)      = f (TyTuple (map (foldType f) ts))
foldType f (TyArray inner)   = f (TyArray (foldType f inner))
foldType f t                 = f t

type Closure = [VarDecl]

data VarDecl = VarDecl { 
    _vdName :: VarIdent,
    _vdUid  :: UniqueId,
    _vdType :: Type,
    _vdClosure :: Closure
  } deriving (Eq, Show)

makeLenses ''VarDecl

instance Ord VarDecl where
  compare = compare `on` view vdUid

type Arguments = [Expression]
data Expression 
  -- c
  = ConstantExpr Const
  -- op a
  | UnaryExpr UnPrimOp Expression
  -- a `op` b
  | BinaryExpr Expression BinPrimOp Expression
  -- if e1 then e2 else e3
  | IfExpr Expression Expression Expression
  -- let x = e1 in e2
  | LetExpr VarIdent Expression Expression
  | ResolvedLetExpr VarDecl Expression Expression
  -- let rec f(x1, x2, x3) = e1 in e2
  | LetRecExpr VarIdent [VarIdent] Expression Expression
  | ResolvedLetRecExpr VarDecl [VarDecl] Expression Expression
  -- x
  | VarExpr VarIdent
  | ResolvedVarExpr VarDecl
  -- f e_1 ... e_n
  | ApplyExpr Expression Arguments
  -- (e_1, e_2, ..., e_n) 
  | TupleExpr [Expression]
  -- Array.create e_1 e_2
  | ArrayCreateExpr Expression Expression
  -- Read from arrays: e_1.(e_2) 
  | ArrayReadExpr Expression Expression
  -- Write to arrays: e_1.(e_2) <- e_3
  | ArrayWriteExpr Expression Expression Expression
  deriving (Eq, Show)

foldExpr :: (Expression -> Expression) -> Expression -> Expression
foldExpr f e@(ConstantExpr _) = f e
foldExpr f (UnaryExpr op e) 
  = f (UnaryExpr op (foldExpr f e))
foldExpr f (BinaryExpr lhs op rhs) 
  = f (BinaryExpr (foldExpr f lhs) op (foldExpr f rhs))
foldExpr f (IfExpr cond th el) 
  = f (IfExpr (foldExpr f cond) (foldExpr f th) (foldExpr f el))
foldExpr f (LetExpr var ass body) 
  = f (LetExpr var (foldExpr f ass) (foldExpr f body))
foldExpr f (ResolvedLetExpr var ass body) 
  = f (ResolvedLetExpr var (foldExpr f ass) (foldExpr f body))
foldExpr f (LetRecExpr var pars body rest) 
  = f (LetRecExpr var pars (foldExpr f body) (foldExpr f rest))
foldExpr f (ResolvedLetRecExpr var pars body rest) 
  = f (ResolvedLetRecExpr var pars (foldExpr f body) (foldExpr f rest))
foldExpr f e@(VarExpr _) = f e
foldExpr f e@(ResolvedVarExpr _) = f e
foldExpr f (ApplyExpr fn args) 
  = f (ApplyExpr (foldExpr f fn) (map (foldExpr f) args))
foldExpr f (TupleExpr es) 
  = f (TupleExpr (map (foldExpr f) es))
foldExpr f (ArrayCreateExpr sz el) 
  = f (ArrayCreateExpr (foldExpr f sz) (foldExpr f el))
foldExpr f (ArrayReadExpr arr index) 
  = f (ArrayReadExpr (foldExpr f arr ) (foldExpr f index))
foldExpr f (ArrayWriteExpr arr index ass) 
  = f (ArrayWriteExpr (foldExpr f arr ) (foldExpr f index) (foldExpr f ass))
