{-# LANGUAGE TupleSections #-}
module Language.Mincaml.Codegen (
  genCode
) where

import Control.Applicative
import Control.Monad
import Control.Monad.Trans(lift)
import Control.Lens

import Data.Word
import Data.Tuple(swap)

import Language.Mincaml.Syntax
import Language.Mincaml.Typing
import Language.Mincaml.Internal.Codegen

import qualified LLVM.General.AST as LA
import qualified LLVM.General.AST.Constant as LAConstant 
import qualified LLVM.General.AST.Instruction as LAInstruction
import qualified LLVM.General.AST.Float as LAFloat 
import qualified LLVM.General.AST.Global as LAGlobal
import qualified LLVM.General.AST.Type as LAType
import qualified LLVM.General.AST.IntegerPredicate as LAIntegerPredicate
import qualified LLVM.General.AST.Operand as LAOperand
import qualified LLVM.General.AST.CallingConvention as LACallingConv
import qualified LLVM.General.AST.AddrSpace as LAAddrSpace


intBits = 32

closureName = LA.Name "#closure"
closureRef  = LA.LocalReference closureName

getBackendType TyBool  = LAType.IntegerType 1
getBackendType TyInt   = LAType.IntegerType intBits
getBackendType TyFloat = LAType.FloatingPointType intBits LAType.DoubleExtended
getBackendType TyUnit  = LAType.IntegerType intBits
getBackendType (TyFunc paramTys retTy)
  = LAType.FunctionType (getBackendType retTy) (map getBackendType paramTys) False
getBackendType (TyTuple elemTys) 
  = LAType.StructureType False (map getBackendType elemTys)
getBackendType (TyArray inner) 
  = LAType.PointerType (getBackendType inner) (LAAddrSpace.AddrSpace 0)
getBackendType e = error $ "Didn't handle " ++ show e

getClosureType :: Closure -> LAType.Type
getClosureType = getBackendType . TyTuple . map (view vdType)


convertConst :: Language.Mincaml.Syntax.Const -> LAConstant.Constant
convertConst (ConstInt n) = LAConstant.Int intBits n
convertConst (ConstFloat n) = LAConstant.Float (LAFloat.Double n)

genExprCode :: Expression -> BlockBuilderM LA.Operand
genExprCode (ConstantExpr c) = return $ LA.ConstantOperand (convertConst c)
genExprCode (BinaryExpr e1 op e2) = do
  instruction <- getInstrByOp op <$> genExprCode  e1 <*> genExprCode e2
  addInstr $ instruction []
  where
    getInstrByOp (BinPlus PKInt)  = LA.Add False False
    getInstrByOp (BinMinus PKInt) = LA.Sub False False
    getInstrByOp (BinMult PKInt)  = LA.Mul False False
    getInstrByOp (BinDiv PKInt)   = LA.SDiv False 
    getInstrByOp BinLess            = LA.ICmp LAIntegerPredicate.SLT
    getInstrByOp BinLessOrEquals    = LA.ICmp LAIntegerPredicate.SLE
    getInstrByOp BinGreater         = LA.ICmp LAIntegerPredicate.SGT
    getInstrByOp BinGreaterOrEquals = LA.ICmp LAIntegerPredicate.SGE
    getInstrByOp BinEquals          = LA.ICmp LAIntegerPredicate.EQ



genExprCode (ResolvedLetExpr varDecl ass body) = do 
  assOp <- genExprCode ass 
  lift $ addDeclValue varDecl assOp
  genExprCode body

genExprCode (ResolvedLetRecExpr funDecl params body rest) = do
  let fullName = LA.Name $ unVarIdent $ view vdName funDecl
  let funRef = LAConstant.GlobalReference fullName
  let funOp = LAOperand.ConstantOperand funRef
  lift $ lift $ do 
    addGlobalDeclValue funDecl funOp
    genFunctionCode fullName params (funDecl ^. vdClosure) body
  genExprCode rest

genExprCode (ResolvedVarExpr decl) = do
  Just val <- lift $ getDeclValue decl  
  return val

genExprCode (IfExpr cond th el) = do
  condOp <- genExprCode cond  
  thBlkName <- lift newBlockName
  elBlkName <- lift newBlockName
  phiBlkName <- lift newBlockName  
  addCurrentBlockWithNewBlockName 
      (LAInstruction.Do $ LAInstruction.CondBr condOp thBlkName elBlkName []) 
      phiBlkName
  thRes <- swap <$> genSingleBody phiBlkName thBlkName th
  elRes <- swap <$> genSingleBody phiBlkName elBlkName el
  addInstr $ LAInstruction.Phi (LAType.IntegerType intBits) [thRes, elRes] []
  where
    genSingleBody phiName blkName expr = lift $ runBlockBuilder blkName $ do
      ret <- genExprCode expr 
      return (ret, LAInstruction.Do $ LAInstruction.Br phiName [])

genExprCode (ApplyExpr fn args) = do
  fnOp <- genExprCode fn
  argOps <- mapM genExprCode args
  addInstr $ LAInstruction.Call False LACallingConv.C [] (Right fnOp) (map (,[]) argOps) [] []


genExprCode (TupleExpr [])               
  = return $ LA.ConstantOperand $ LAConstant.Int intBits 0
genExprCode (TupleExpr [inner]) = genExprCode inner
genExprCode e@(TupleExpr elems) = do
  elemOps <- mapM genExprCode elems
  let eTy = getBackendType $ getType e
  let firstOperand = LAOperand.ConstantOperand $ LAConstant.Undef eTy
  foldM 
    (\aggOp (n, elOp) -> addInstr $ LAInstruction.InsertValue aggOp elOp [n] [])
    firstOperand 
    (zip [0..] elemOps)
  

genFunctionCode :: LA.Name -> [VarDecl] -> Closure -> Expression -> GlobalCodegenM ()
genFunctionCode fullName params closure body 
  = runCodegen fullName llvmParams (getBackendType (getType body)) $ do
    mapM_ (\x -> addDeclValue x (LAOperand.LocalReference (getName x))) params
    newBlockName >>= flip runBlockBuilder (do
      -- Init closures FIXME: make it work
      -- mapM_ initClosure (zip [0..] closure)
      retOp <- genExprCode body
      return ((), LA.Do $ LA.Ret (Just retOp) []))
    return ()
    where
      initClosure (n, decl) 
        = addInstr (LAInstruction.ExtractValue closureRef [n] []) >>= lift . addDeclValue decl
      closureParam = LA.Parameter (getClosureType closure) closureName []
      llvmParams   = {-closureParam:-} map mkParam params
      mkParam decl = LA.Parameter (getDeclType decl) (getName decl) []
      getName      = LA.Name . unVarIdent . view vdName
      getDeclType  = getBackendType . view vdType
    

genPrintFunctions :: GlobalCodegenM ()
genPrintFunctions = do
  let printIntDecl = VarDecl (VarIdent "printInt") (UniqueId 0) (TyFunc [TyInt] TyUnit) []  
  let func = LAGlobal.functionDefaults { 
    LAGlobal.name = LA.Name "printInt",
    LAGlobal.returnType = LAType.IntegerType intBits,
    LAGlobal.parameters = ([LA.Parameter (LAType.IntegerType intBits) (LA.Name "p") []], False),
    LAGlobal.basicBlocks = [] }
  addFunctionDefinition $ LA.GlobalDefinition func
  addGlobalDeclValue printIntDecl (LAOperand.ConstantOperand (LAConstant.GlobalReference (LA.Name "printInt")))


genCode :: Expression -> LA.Module
genCode expr = runGlobalCodegen $ genPrintFunctions >> genFunctionCode (LA.Name "main") [] [] expr
