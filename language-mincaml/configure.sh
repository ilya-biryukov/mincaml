#!/bin/bash

set -e

CABAL_FLAGS=--enable-tests
cabal install -j --only-dependencies $CABAL_FLAGS 
cabal configure $CABAL_FLAGS -p --enable-executable-profiling
