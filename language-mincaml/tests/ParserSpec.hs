module ParserSpec where

import Test.Hspec

import Language.Mincaml.Parser
import Language.Mincaml.Syntax
import Text.Parsec.Prim

shouldBeRight :: (Eq b, Show a, Show b) => Either a b -> b -> Expectation
shouldBeRight (Left x)  _ = expectationFailure ("Expected Right, but got Left " ++ show x)
shouldBeRight (Right x) v = x `shouldBe` v

spec :: Spec
spec =
  describe "Expression parsing" $ do
    let p = parse parseMinCaml ""
    it "should fail on empty input" $ do
      p "" `shouldSatisfy` isLeft
    it "should parse numbers" $ do
      p "100" `shouldBeRight` cexpr
      p "100.0" `shouldBeRight` (ConstantExpr $ ConstFloat 100.0)
    it "should parse tuples" $ do
      p "(100, 100, 100)" `shouldBeRight` TupleExpr (replicate 3 cexpr)
      p "(100)" `shouldBeRight` TupleExpr [cexpr]
    it "should parse if" $ do
      p "if 100 then 100 else 100" `shouldBeRight` IfExpr cexpr cexpr cexpr
      p "if 100 then if 100 then 100 else 100 else 100"
        `shouldBeRight` IfExpr cexpr (IfExpr cexpr cexpr cexpr) cexpr
    it "should parse application" $ do
      p "100 100" `shouldBeRight` ApplyExpr cexpr [cexpr]
      p "100 100 100 100" `shouldBeRight` ApplyExpr cexpr (replicate 3 cexpr)
    it "should parse variable" $ do
      p "x" `shouldBeRight` x
    it "should parse let" $ do
      p "let x = 100 in x" `shouldBeRight` LetExpr xvar cexpr x
    it "should parse array creation" $ do
      p "Array.create 100 100" `shouldBeRight` ArrayCreateExpr cexpr cexpr
      p "Array.create 100 100 100" `shouldSatisfy` isLeft
    it "should parse array read" $ do
      p "x.(100)" `shouldBeRight` ArrayReadExpr x cexpr
      p "x.(100).(100)" `shouldBeRight` ArrayReadExpr (ArrayReadExpr x cexpr) cexpr
      p "x . ( 100 )" `shouldBeRight` ArrayReadExpr x cexpr
    it "should parse array write" $ do
      p "x.(100)<-100 " `shouldBeRight` ArrayWriteExpr x cexpr cexpr
      p "x . ( 100 ) <- 100 " `shouldBeRight` ArrayWriteExpr x cexpr cexpr
      p "x . ( 100 ).(100) <- 100 " 
        `shouldBeRight` ArrayWriteExpr (ArrayReadExpr x cexpr) cexpr cexpr
    it "should parse binary expressions" $ do
      p "x - x + x" `shouldBeRight` BinaryExpr (BinaryExpr x opMinus x) opPlus x
      p "x * x + x" `shouldBeRight` BinaryExpr (BinaryExpr x opMult x) opPlus x
      p "x + 100 + 100" 
        `shouldBeRight` BinaryExpr (BinaryExpr x opPlus cexpr) opPlus cexpr
      p "x <= x + 100" 
        `shouldBeRight` BinaryExpr x BinLessOrEquals (BinaryExpr x opPlus cexpr)
    it "should parse unary expressions" $ do
      p "-x" `shouldBeRight` UnaryExpr UnMinus x
      p "! (-x)" `shouldBeRight` UnaryExpr UnNot (TupleExpr [UnaryExpr UnMinus x])
    it "should resolve a few ambigs" $ do    
      p "x x.(x)" `shouldBeRight` ApplyExpr x [ArrayReadExpr x x]
      p "x x.(x) <- 100" `shouldSatisfy` isLeft
    it "should parse preceding spaces" $ do
      p "   x" `shouldBeRight` x
    it "should parse let rec" $ do
      p "let rec f(x) = x in f x" `shouldBeRight` 
        LetRecExpr (VarIdent "f") [xvar] x (ApplyExpr (VarExpr (VarIdent "f")) [x])


  where
    opPlus  = BinPlus PKInt
    opMinus = BinMinus PKInt
    opMult  = BinMult PKInt
    cexpr   = ConstantExpr $ ConstInt 100
    xvar    = VarIdent "x"
    x       = VarExpr xvar
    isLeft  = either (const True) (const False)
    shouldMatchGold inFile goldFile action = do
      inData <- readFile inFile
      goldData <- readFile goldFile
      action inData `shouldBe` goldData
