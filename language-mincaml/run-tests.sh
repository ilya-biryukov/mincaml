#!/bin/bash

set -e

cabal build tests
./dist/build/tests/tests "$@"
